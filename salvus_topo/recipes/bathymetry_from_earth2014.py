"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import pathlib
import typing

import click

from ..remote_data import fetch
from ..sphere_surf_model import SphereSurfModel

grid = "gauss"
max_lmax = 10800

lmaxmax_topo = 10800

min_bathymetry = 0.0


def custom(output_filename: pathlib.Path, lmax_degrees: typing.List[int]):
    click.echo("Checking Earth 2014 TBI (Topo, Bedrock, Ice) file ...")
    path_TBI = fetch("EARTH_2014/Earth2014.TBI2014.degree10800.bshc")[
        "Earth2014.TBI2014.degree10800.bshc"
    ]
    click.echo("Checking Earth 2014 SUR (Surface) file ...")
    path_SUR = fetch("EARTH_2014/Earth2014.SUR2014.degree10800.bshc")[
        "Earth2014.SUR2014.degree10800.bshc"
    ]

    for _i, lmax in enumerate(lmax_degrees):
        click.echo(
            f"Computing model for l_max {lmax} ({_i + 1} of "
            f"{len(lmax_degrees)})"
        )

        lmax_transform = min(lmax * 2, lmaxmax_topo)

        nphi = lmax * 4 + 4
        ntheta = lmax * 2 + 4

        click.echo("    -> Reading TBI ...")
        TBI = SphereSurfModel.read_Earth2014_shc(
            path_TBI, lmax_transform, grid="gauss", nphi=nphi, ntheta=ntheta
        )
        click.echo("    -> Reading SUR ...")
        SUR = SphereSurfModel.read_Earth2014_shc(
            path_SUR, lmax_transform, grid="gauss", nphi=nphi, ntheta=ntheta
        )

        click.echo("    -> Computing difference ...")
        bathymetry = SUR - TBI

        if lmax < lmaxmax_topo:
            click.echo("    -> Spectral filtering ...")
            bathymetry.filter_model(lmax, lmax_transform)

        bathymetry.data_space[bathymetry.data_space < min_bathymetry] = 0.0

        bathymetry.rotate_regdata_180()

        click.echo("    -> Writing to file ...")
        bathymetry.write_cf(
            output_filename, vname=f"{output_filename.stem}_lmax_{lmax}"
        )
