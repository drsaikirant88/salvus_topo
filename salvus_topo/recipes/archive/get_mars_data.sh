#!/bin/bash

mkdir data
cd data

# Marc Wieczoreks crustal models
# https://www.dropbox.com/sh/azjzeph273a8woj/AAAAL0cTy6dt3YCoR3BG9yY_a?dl=0
wget -O mars.zip https://www.dropbox.com/sh/azjzeph273a8woj/AAATO6C0KrczhgtmyXEYxW3ka/Reference%20Crustal%20Thickness%20Models/DWTh2Ref1?dl=1
unzip mars.zip
rm mars.zip

# http://pds-geosciences.wustl.edu/missions/mgs/megdr.html
wget -O meg004_megr90n000cb.img http://pds-geosciences.wustl.edu/mgs/mgs-m-mola-5-megdr-l3-v1/mgsl_300x/meg004/megr90n000cb.img
wget -O meg016_megr90n000cb.img http://pds-geosciences.wustl.edu/mgs/mgs-m-mola-5-megdr-l3-v1/mgsl_300x/meg016/megr90n000eb.img
wget -O meg032_megr90n000cb.img http://pds-geosciences.wustl.edu/mgs/mgs-m-mola-5-megdr-l3-v1/mgsl_300x/meg032/megr90n000fb.img

wget -O meg064_megr90n000cb.img http://pds-geosciences.wustl.edu/mgs/mgs-m-mola-5-megdr-l3-v1/mgsl_300x/meg064/megr90n000gb.img
wget -O meg064_megr90n180cb.img http://pds-geosciences.wustl.edu/mgs/mgs-m-mola-5-megdr-l3-v1/mgsl_300x/meg064/megr90n180gb.img
wget -O meg064_megr00n000cb.img http://pds-geosciences.wustl.edu/mgs/mgs-m-mola-5-megdr-l3-v1/mgsl_300x/meg064/megr00n000gb.img
wget -O meg064_megr00n180cb.img http://pds-geosciences.wustl.edu/mgs/mgs-m-mola-5-megdr-l3-v1/mgsl_300x/meg064/megr00n180gb.img

cd ..
