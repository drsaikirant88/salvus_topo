"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
from __future__ import division, print_function
import os
import inspect
import numpy as np
import matplotlib.pyplot as plt
from sht import sht
import scipy.misc

from sphere_surf_model import SphereSurfModel

DATA_DIR = os.path.join(
    os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))),
    "tides_data",
)

grid = "gauss"
# fname_tpx08 = 'hf.m2_tpxo8_atlas_30c_v1.nc'
fname_tpx09 = "h_m2_tpxo9_atlas_30.nc"
llmax = [16, 32, 64, 128, 256, 512, 1024]  # , 2048, 4096, 5400]
lmaxmax = 5400
cf_kwargs = {
    "fmt": "NETCDF4",
    "compress": 2,
    "mode": "w",
    "single": True,
    "HDF_chunksizes": {0: 1024, 1: 1024},
}
fname_out = "tidal_elevation_filtered_TPX09_m2"
plot = True
show = False
pdf = False
jpg = True
fancy = True

min_amp = 1e-4

path_tpx09 = os.path.join(DATA_DIR, fname_tpx09)

print("read TPX09")
re = SphereSurfModel.read_tpx08(path_tpx09, var="hRe")
im = SphereSurfModel.read_tpx08(path_tpx09, var="hIm")

print("interpolate to gauss grid")
# just generate the theta grid
sh = sht(4, re.nphi + 2, re.ntheta + 1, grid=grid)

re.spline_interp_new_grid(phi=sh.phi, theta=sh.theta, grid=grid)
im.spline_interp_new_grid(phi=sh.phi, theta=sh.theta, grid=grid)

# print('downsampling')
# re.downsample(2)
# im.downsample(2)

for lmax in llmax:
    print(lmax)
    lmax_transform = min(lmax * 2, lmaxmax)

    nphi = lmax * 4 + 4
    ntheta = lmax * 2 + 4

    re_l = re.copy()
    im_l = im.copy()
    if lmax < lmaxmax:
        print("filter")
        re_l.filter_model(lmax, lmax_transform)
        im_l.filter_model(lmax, lmax_transform)

    re_l.spline_interp_new_grid(nphi=nphi, ntheta=ntheta, grid="regular")
    im_l.spline_interp_new_grid(nphi=nphi, ntheta=ntheta, grid="regular")

    amp = re_l.copy()
    amp.data_space = (re_l.data_space ** 2 + im_l.data_space ** 2) ** 0.5

    grad = amp.copy()
    grad.spline_interp_new_grid(
        nphi=nphi, ntheta=ntheta, grid="regular", abs_gradient=True
    )

    if plot:
        print("plotting")
        if pdf or show:
            re_l.plot(
                show=False,
                vmin=re.data_space.min(),
                vmax=re.data_space.max(),
                cblabel="re",
            )
            plt.savefig(fname_out + "_re_lmax_%d.pdf" % lmax)

            im_l.plot(
                show=False,
                vmin=im.data_space.min(),
                vmax=im.data_space.max(),
                cblabel="im",
            )
            plt.savefig(fname_out + "_im_lmax_%d.pdf" % lmax)

            amp.plot(
                show=False, vmin=0.0, vmax=re.data_space.ptp(), cblabel="im"
            )
            plt.savefig(fname_out + "_im_lmax_%d.pdf" % lmax)

        if jpg:
            scipy.misc.imsave(
                fname_out + "_re_lmax_%d.jpg" % lmax, re_l.data_space.T
            )
            scipy.misc.imsave(
                fname_out + "_im_lmax_%d.jpg" % lmax, im_l.data_space.T
            )
            scipy.misc.imsave(
                fname_out + "_amp_lmax_%d.jpg" % lmax, amp.data_space.T
            )
            scipy.misc.imsave(
                fname_out + "_grad_lmax_%d.jpg" % lmax, grad.data_space.T
            )

        if fancy:
            ss = np.argsort(amp.data_space.ravel())
            dn = np.zeros_like(ss)
            dn[ss] = np.arange(ss.size)
            dn = dn.reshape(amp.shape)
            sl = amp.data_space <= min_amp
            dn[sl] = sl.sum()
            scipy.misc.imsave("tidal_loading_fancy_%d.png" % lmax, dn.T)

    re_l.rotate_regdata_180()
    im_l.rotate_regdata_180()
    amp.rotate_regdata_180()
    grad.rotate_regdata_180()

    re_l.write_cf(
        fname_out + ".nc",
        vname="tidal_elevation_re_lmax_%d" % lmax,
        cf_kwargs=cf_kwargs,
    )
    cf_kwargs["mode"] = "a"
    im_l.write_cf(
        fname_out + ".nc",
        vname="tidal_elevation_im_lmax_%d" % lmax,
        cf_kwargs=cf_kwargs,
    )
    amp.write_cf(
        fname_out + ".nc",
        vname="tidal_elevation_amp_lmax_%d" % lmax,
        cf_kwargs=cf_kwargs,
    )
    grad.write_cf(
        fname_out + ".nc",
        vname="tidal_elevation_grad_lmax_%d" % lmax,
        cf_kwargs=cf_kwargs,
    )

if show:
    plt.show()
