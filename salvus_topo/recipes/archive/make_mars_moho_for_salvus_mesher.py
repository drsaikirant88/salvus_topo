"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import os
import inspect
import numpy as np
import matplotlib.pyplot as plt
from sht import sht
import scipy.misc

from sphere_surf_model import SphereSurfModel

DATA_DIR = os.path.join(
    os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))),
    "data",
)

grid = "gauss"
fname_moho = "3000_5_DWTh2Ref1_moho.dat"

cf_kwargs = {
    "fmt": "NETCDF4",
    "compress": 2,
    "mode": "w",
    "single": True,
    "HDF_chunksizes": {0: 1024, 1: 1024},
}

llmax = [16, 32, 64, 128, 180]
lmaxmax_moho = 178

fname_out = "mars_moho_filtered"
plot = True
show = True
pdf = False
jpg = True
fancy = True

path_moho = os.path.join(DATA_DIR, fname_moho)
moho = SphereSurfModel.read_crustal_thickness_wieczorek_dat(path_moho)

# just generate the theta grid
sh = sht(4, moho.nphi, moho.ntheta, grid=grid)

# interpolate to gauss grid with same number of samples to enable sht
print("interp moho")
moho.spline_interp_new_grid(phi=sh.phi, theta=sh.theta, grid=grid)

for lmax in llmax:
    print(lmax)
    lmax_transform = min(lmax * 2, lmaxmax_moho)

    nphi = lmax * 4 + 4
    ntheta = lmax * 2 + 4

    moho_l = moho.copy()
    if lmax < lmaxmax_moho:
        print("filter moho")
        moho_l.filter_model(lmax, lmax_transform)

    print("ds moho")
    phi = np.linspace(0, 2 * np.pi, nphi, endpoint=False)
    theta = np.linspace(0, np.pi, ntheta, endpoint=False)
    theta += theta[1] / 2.0
    moho_l.spline_interp_new_grid(phi=phi, theta=theta, grid="regular")

    print("reference to ellipsoid")
    elli = SphereSurfModel.ellipsoid(
        "MARS", radius=3389500.0, phi=phi, theta=theta, grid="regular"
    )

    elli.data_space += 3389500.0
    moho_l = moho_l - elli

    moho_l.rotate_regdata_180()
    if plot:
        print("plotting")
        if pdf or show:
            moho_l.plot(show=False, cblabel="moho_tography relative to ELLI")
            plt.savefig(fname_out + "_lmax_%d.pdf" % lmax)

        if jpg:
            scipy.misc.imsave(
                fname_out + "_lmax_%d.jpg" % lmax, moho_l.data_space.T
            )

        if fancy:
            ss = np.argsort(moho_l.data_space.ravel())
            dn = np.zeros_like(ss)
            dn[ss] = np.arange(ss.size)
            dn = dn.reshape(moho_l.shape)
            scipy.misc.imsave("dnt_mars_moho.png", dn.T)

    # rotate, because in salvus_mesher we need to start from longitude 0.
    moho_l.rotate_regdata_180()

    print("write")
    moho_l.write_cf(
        fname_out + ".nc",
        vname=fname_out + "_lmax_%d" % lmax,
        cf_kwargs=cf_kwargs,
    )
    cf_kwargs["mode"] = "a"

plt.show()
