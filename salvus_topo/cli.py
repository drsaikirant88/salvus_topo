"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import pathlib
import sys
import typing

import click

from .sphere_surf_model import SphereSurfModel

POSSIBLE_LMAX_VALUES = [16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 10800]


@click.group()
@click.option("--pdb", help="Enable post-mortem debugger.", is_flag=True)
def main(pdb):
    """
    SalvusTopo
    """
    # Enable post-mortem debugger hook.
    if pdb:

        def info(type, value, tb):
            if hasattr(sys, "ps1") or not sys.stderr.isatty():
                # we are in interactive mode or we don't have a tty-like
                # device, so we call the default hook
                sys.__excepthook__(type, value, tb)
            else:
                import traceback, pdb as debugger  # NOQA

                traceback.print_exception(type, value, tb)
                debugger.post_mortem(tb)

        sys.excepthook = info


def _run_recipe(
    model: SphereSurfModel,
    lmax_degrees: typing.List[int],
    output_filename: pathlib.Path,
):
    for _i, lmax in enumerate(lmax_degrees):
        click.echo(
            f"Computing model for l_max {lmax} ({_i + 1} of "
            f"{len(lmax_degrees)})"
        )
        nphi = lmax * 8
        ntheta = lmax * 4
        lmax_transform = lmax * 2

        # do the filtering
        click.echo("    -> Spectral filtering ...")
        model_l = model.copy()
        model_l.filter_model(lmax, lmax_transform)

        click.echo("    -> Interpolation ...")
        model_l.spline_interp_new_grid(
            nphi=nphi, ntheta=ntheta, order=1,
        )

        # rotate, because we need to start from longitude 0 in SalvusMesh.
        model_l.rotate_regdata_180()

        click.echo("    -> Writing to file ...")
        model_l.write_cf(
            output_filename, vname=f"{output_filename.stem}_lmax_{lmax}"
        )


from .recipes import (  # NOQA
    moho_topography_from_crust_1_0_and_egm2008,
    bathymetry_from_earth2014,
    topography_from_earth2014_and_egm2008,
)

all_recipes = {
    r.__name__.split(".")[-1].replace("_", "-"): r
    for r in [
        moho_topography_from_crust_1_0_and_egm2008,
        bathymetry_from_earth2014,
        topography_from_earth2014_and_egm2008,
    ]
}


@main.command("create")
@click.option(
    "--output-filename",
    type=click.Path(),
    required=True,
    help="The output filename. Must not exist yet.",
)
@click.option(
    "--max-lmax",
    type=click.Choice(str(i) for i in POSSIBLE_LMAX_VALUES),
    required=True,
    help="The maximum spherical harmonic degree to "
    "generate. The higher the degree, the more detailed the model. "
    "High degrees might demand a lot of memory.",
)
@click.argument(
    "model", type=click.Choice(sorted(all_recipes.keys())), metavar="MODEL"
)
def _function(max_lmax, model, output_filename):

    max_lmax = int(max_lmax)

    output_filename = pathlib.Path(output_filename)
    if output_filename.exists():
        raise ValueError("Output file must not yet exist.")

    output_filename.parent.mkdir(exist_ok=True)

    assert hasattr(all_recipes[model], "max_lmax")

    if max_lmax > all_recipes[model].max_lmax:
        raise ValueError(
            "The chosen model has a max lmax of "
            f"{all_recipes[model].max_lmax}. It does not contain enough "
            "detail to warrant generating a higher degree."
        )

    lmax_degrees = [i for i in POSSIBLE_LMAX_VALUES if i <= max_lmax]

    if "get_spherical_model" in dir(all_recipes[model]):
        m = all_recipes[model].get_spherical_model()
        _run_recipe(
            model=m,
            lmax_degrees=lmax_degrees,
            output_filename=output_filename,
        )
    elif "custom" in dir(all_recipes[model]):
        all_recipes[model].custom(
            output_filename=output_filename, lmax_degrees=lmax_degrees
        )
    else:  # pragma: no cover
        raise NotImplementedError
