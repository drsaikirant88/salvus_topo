"""
Tests for the sht class.

(c) Mondaic AG (info@mondaic.com), 2020
"""
import numpy as np
from sht import sht


def test_sht_transform():
    nphi, ntheta = 16, 8
    lmax = 4

    # regular grid
    sh = sht(lmax, nphi, ntheta, load_save_cfg=False)

    data = np.ones((nphi, ntheta))
    ylm = sh.sht(data)
    data_trans = sh.isht(ylm)
    np.testing.assert_allclose(data, data_trans)

    tm, pm = np.meshgrid(sh.theta, sh.phi)
    data = np.sin(tm) * np.cos(pm)
    ylm = sh.sht(data)
    data_trans = sh.isht(ylm)
    np.testing.assert_allclose(data, data_trans, atol=1e-7)

    # gauss grid
    sh = sht(lmax, nphi, ntheta, load_save_cfg=False, grid="gauss")
    tm, pm = np.meshgrid(sh.theta, sh.phi)
    data = np.sin(tm) * np.cos(pm)
    ylm = sh.sht(data)
    data_trans = sh.isht(ylm)
    np.testing.assert_allclose(data, data_trans, atol=1e-7)

    # test quick init for lmax >= 32
    lmax = 32
    nphi = 4 * lmax
    ntheta = lmax * 2

    sh = sht(
        lmax, nphi, ntheta, load_save_cfg=False, grid="gauss", quick_init=True
    )
    tm, pm = np.meshgrid(sh.theta, sh.phi)
    data = np.sin(tm) * np.cos(pm)
    ylm = sh.sht(data.copy())
    data_trans = sh.isht(ylm)
    np.testing.assert_allclose(data, data_trans, atol=1e-7)


def test_sht_rotate():
    nphi, ntheta = 16, 8
    lmax = 4

    # regular grid
    sh = sht(lmax, nphi, ntheta, load_save_cfg=False)

    tm, pm = np.meshgrid(sh.theta, sh.phi)
    data = np.sin(tm) * np.cos(pm)
    ylm = sh.sht(data)
    ylm = sh.rotate_ylm(ylm, (180.0, None, None))
    data_trans = sh.isht(ylm)

    np.testing.assert_allclose(-data, data_trans, atol=1e-7)
