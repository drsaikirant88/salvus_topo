"""
Tests for the SphereSurfModel class.

(c) Mondaic AG (info@mondaic.com), 2020
"""
import inspect
import numpy as np
import os
from sphere_surf_model import SphereSurfModel

# Most generic way to get the data directory.
DATA_DIR = os.path.join(
    os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))),
    "data",
)


def test_add():
    d1 = SphereSurfModel.ellipsoid(nphi=4, ntheta=10)
    d2 = SphereSurfModel.ellipsoid(nphi=4, ntheta=10)
    d2.data_space = -d2.data_space

    d3 = d1 + d2

    np.testing.assert_allclose(
        d3.data_space.ravel(), np.zeros(4 * 10), atol=1e-7
    )


def test_sub():
    d1 = SphereSurfModel.ellipsoid(nphi=4, ntheta=10)
    d2 = SphereSurfModel.ellipsoid(nphi=4, ntheta=10)

    d3 = d1 - d2

    np.testing.assert_allclose(
        d3.data_space.ravel(), np.zeros(4 * 10), atol=1e-7
    )


def test_ellipsoid():
    elli = SphereSurfModel.ellipsoid(nphi=4, ntheta=10)
    data_ref = np.array(
        [
            -13727.71858542,
            -3583.06461585,
            6610.31003763,
            -9858.53154571,
            -9858.53154571,
            6610.31003763,
        ]
    )

    np.testing.assert_allclose(
        elli.data_space.flatten()[::7], data_ref, atol=1e-7
    )


def test_read_Earth2014_grid():
    fname = os.path.join(
        DATA_DIR, "Earth2014Shape_minus_6371000m.TBI2014.5min.geod.bin"
    )
    topo = SphereSurfModel.read_Earth2014_grid(fname)

    data_ref = np.array(
        [
            -18642.0,
            -5395.0,
            520.0,
            -12198.0,
            -4802.0,
            7334.0,
            -14448.0,
            -9013.0,
            7705.0,
            -4724.0,
            -13158.0,
            -1033.0,
            -3484.0,
            -11326.0,
        ]
    )

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        data_ref,
        atol=1e-7,
    )


def test_read_Earth2014_shc():

    data_ref = np.array(
        [
            -3246.28527102,
            -4952.4436339,
            -1785.3804665,
            -5282.15720636,
            231.00811248,
            1955.98365165,
            -3682.78975412,
            -311.71175138,
            2382.38338207,
            -3150.31353575,
            305.81065407,
            -4590.12838784,
            -4777.35935979,
            969.41622125,
        ]
    )

    fname = os.path.join(DATA_DIR, "Earth2014.TBI2014.degree2160.bshc")
    topo = SphereSurfModel.read_Earth2014_shc(fname, 16, grid="gauss")

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        data_ref,
        atol=1e-7,
    )

    fname = os.path.join(DATA_DIR, "Earth2014.TBI2014.degree10800.bshc")
    topo = SphereSurfModel.read_Earth2014_shc(fname, 16, grid="gauss")

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        data_ref,
        atol=1e-7,
    )


def test_read_crust1():
    fname = os.path.join(DATA_DIR, "crust1.bnds")
    crust = SphereSurfModel.read_crust1(fname, index="ocean")

    data_ref = np.array(
        [
            3.69,
            5.54,
            4.26,
            0.09,
            0.0,
            3.6,
            0.05,
            5.43,
            1.77,
            0.0,
            0.0,
            0.06,
            5.36,
            0.0,
        ]
    )

    np.testing.assert_allclose(
        crust.data_space.flatten()[:: crust.data_space.size / 13],
        data_ref * 1e3,
        atol=1e-7,
    )


def test_read_EGM2008_undulations():
    fname = os.path.join(
        DATA_DIR, "Und_min2.5x2.5_egm2008_isw=82_WGS84_TideFree_SE"
    )
    topo = SphereSurfModel.read_EGM2008_undulations(fname)

    data_ref = np.array(
        [
            14.89850235,
            0.90196216,
            -19.52381325,
            -17.98793602,
            -7.21594858,
            35.50471878,
            18.20285988,
            33.73376083,
            14.26233768,
            -70.08519745,
            -29.10905838,
            -24.31369781,
            32.79760361,
            -30.1499691,
        ]
    )

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        data_ref,
        atol=1e-7,
    )


def test_downsample():
    fname = os.path.join(
        DATA_DIR, "Earth2014Shape_minus_6371000m.TBI2014.5min.geod.bin"
    )
    topo = SphereSurfModel.read_Earth2014_grid(fname)
    topo.downsample(16)

    data_ref = np.array(
        [
            -17468.73828125,
            -9180.1015625,
            2527.96484375,
            -354.98828125,
            -13776.00390625,
            -11449.26171875,
            2929.04296875,
            3004.8203125,
            -11974.27734375,
            -12422.93359375,
            -1670.27734375,
            -427.07421875,
            -8421.15234375,
            -13473.35546875,
        ]
    )

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        data_ref,
        atol=1e-7,
    )


def test_rotate_ylm():
    fname = os.path.join(DATA_DIR, "Earth2014.TBI2014.degree2160.bshc")
    topo = SphereSurfModel.read_Earth2014_shc(fname, 16, grid="gauss")
    topo.rotate_ylm(euler_angles=(180.0, 0.0, 0.0))

    topo2 = SphereSurfModel.read_Earth2014_shc(fname, 16, grid="gauss")
    topo2.rotate_regdata_180()

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        topo2.data_space.flatten()[:: topo.data_space.size / 13],
        atol=1e-7,
    )


def test_rotate_regdata_180():
    fname = os.path.join(
        DATA_DIR, "Earth2014Shape_minus_6371000m.TBI2014.5min.geod.bin"
    )
    topo = SphereSurfModel.read_Earth2014_grid(fname)
    topo.rotate_regdata_180()

    data_ref = np.array(
        [
            -18641.0,
            -2335.0,
            2243.0,
            -9719.0,
            -3851.0,
            6911.0,
            -12281.0,
            -9705.0,
            1847.0,
            -7810.0,
            -12693.0,
            -1039.0,
            -4237.0,
            -11516.0,
        ]
    )

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        data_ref,
        atol=1e-7,
    )


def test_spline_interp_new_grid():
    fname = os.path.join(
        DATA_DIR, "Earth2014Shape_minus_6371000m.TBI2014.5min.geod.bin"
    )
    topo = SphereSurfModel.read_Earth2014_grid(fname)
    topo.downsample(8)
    data_ref = topo.data_space.flatten()[:: topo.data_space.size / 13]
    topo.spline_interp_new_grid(phi=topo.phi, theta=topo.theta)

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        data_ref,
        atol=1e-7,
    )


def test_filter_model():
    fname = os.path.join(
        DATA_DIR, "Earth2014Shape_minus_6371000m.TBI2014.5min.geod.bin"
    )
    topo = SphereSurfModel.read_Earth2014_grid(fname)
    topo.grid = "regular"
    topo.downsample(8)
    topo.filter_model(32, filter_type="gauss")

    data_ref = np.array(
        [
            -17191.11519788,
            1865.28521369,
            -14398.74956506,
            589.40666179,
            -9878.15611856,
            -3634.53001579,
            -7943.84581321,
            -8548.41043816,
            482.35222472,
            -12593.88664612,
            4765.93760911,
            -9692.44000114,
            1792.22629376,
            -12050.48728689,
        ]
    )

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        data_ref,
        atol=1e-7,
    )

    topo = SphereSurfModel.read_Earth2014_grid(fname)
    topo.grid = "regular"
    topo.downsample(8)
    topo.filter_model(32, filter_type="butterworth")

    data_ref = np.array(
        [
            -17753.81233698,
            1818.90990476,
            -14481.53469024,
            521.15394228,
            -9827.36794912,
            -3959.00140897,
            -8212.92388752,
            -8646.34982161,
            599.26892047,
            -13033.28618599,
            4675.93302404,
            -9369.68679517,
            1663.01936435,
            -11942.13355611,
        ]
    )

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        data_ref,
        atol=1e-7,
    )

    topo = SphereSurfModel.read_Earth2014_grid(fname)
    topo.grid = "regular"
    topo.downsample(8)
    topo.filter_model(32, filter_type="cutoff")

    data_ref = np.array(
        [
            -17815.97228854,
            1696.89952417,
            -14743.82516845,
            770.02056691,
            -10118.19331675,
            -4111.76512023,
            -8340.09633945,
            -8378.38761251,
            743.69636266,
            -13207.10778052,
            4952.14470013,
            -9841.55248364,
            1610.08843214,
            -12318.27790524,
        ]
    )

    np.testing.assert_allclose(
        topo.data_space.flatten()[:: topo.data_space.size / 13],
        data_ref,
        atol=1e-7,
    )


def test_compute_spectral_power():
    fname = os.path.join(
        DATA_DIR, "Earth2014Shape_minus_6371000m.TBI2014.5min.geod.bin"
    )
    topo = SphereSurfModel.read_Earth2014_grid(fname)
    topo.grid = "regular"
    topo.downsample(8)
    power = topo.compute_spectral_power()

    data_ref = np.array(
        [
            5.53378204e06,
            4.05473555e02,
            5.71382768e01,
            1.71083072e01,
            6.51772629e00,
            3.21373009e00,
            2.05447573e00,
            1.37803019e00,
            8.82372095e-01,
            5.78413844e-01,
            3.99850501e-01,
            2.74509286e-01,
            2.13323486e-01,
            1.79078173e-01,
        ]
    )

    np.testing.assert_allclose(power[:: power.size / 13], data_ref, atol=1e-7)
