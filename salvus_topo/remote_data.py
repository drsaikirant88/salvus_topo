"""
(c) Mondaic AG (info@mondaic.com), 2020
"""
import pathlib
import typing

import pooch
from pooch import HTTPDownloader, Untar, Decompress


SALVUS_TOPO_DATA = pooch.create(
    path=pooch.os_cache("salvus_topo"),
    base_url="https://data.mondaic.com",
    registry={
        "CRUST_1.0/crust1.0.tar.gz": "0b41b46fc3e1a76debbbcb66ab407febaeee4dc3033db7a0a24d2bb7c7adfe3e",  # NOQA
        "EARTH_2014/Earth2014.TBI2014.degree10800.bshc": "72ffb432d268ae6972d3251ef395b980b38e3313627c6c981a0197882efb4f4c",  # NOQA
        "EARTH_2014/Earth2014.SUR2014.degree10800.bshc": "f9cc0209cffeb1faef58ec9d03886941448bfffd9e36f5ee56c51063c3f778f9",  # NOQA
        "EGM2008/Und_min2.5x2.5_egm2008_isw=82_WGS84_TideFree_SE.gz": "1a12a1ad453d274084675966038d7394f9446daa5139e4d9a9dfcf8a9959bc64",  # NOQA
    },
    # Now specify custom URLs for some of the files in the registry.
    urls={
        # Crust 1.0.
        "CRUST_1.0/crust1.0.tar.gz": "http://igppweb.ucsd.edu/~gabi/crust1/crust1.0.tar.gz",  # NOQA
        # Earth 2014 1 arcmin shape grid TBI (topography, bedrock and ice =
        # Earth without water masses)
        "EARTH_2014/Earth2014.TBI2014.degree10800.bshc": "http://ddfe.curtin.edu.au/models/Earth2014/data_1min/shcs_to10800/Earth2014.TBI2014.degree10800.bshc",  # NOQA
        # Earth 2014 1 arcmin shc SUR
        "EARTH_2014/Earth2014.SUR2014.degree10800.bshc": "http://ddfe.curtin.edu.au/models/Earth2014/data_1min/shcs_to10800/Earth2014.SUR2014.degree10800.bshc",  # NOQA
        # Geoid.
        "EGM2008/Und_min2.5x2.5_egm2008_isw=82_WGS84_TideFree_SE.gz": "http://earth-info.nga.mil/GandG/wgs84/gravitymod/egm2008/Small_Endian/Und_min2.5x2.5_egm2008_isw=82_WGS84_TideFree_SE.gz",  # NOQA
    },
)


def fetch(key: str) -> typing.Dict[str, pathlib.Path]:
    downloader = HTTPDownloader(progressbar=True)
    processor = None
    if key.endswith("tar.gz"):
        processor = Untar()
    elif key.endswith(".gz"):
        processor = Decompress()
    fname = SALVUS_TOPO_DATA.fetch(
        key, downloader=downloader, processor=processor
    )

    if not isinstance(fname, list):
        fname = [fname]

    file_dict = {}
    # Currently assumes a flat layout - let's see ...
    for f in fname:
        f = pathlib.Path(f)
        assert f.exists(), f
        file_dict[f.name] = f

    return file_dict
